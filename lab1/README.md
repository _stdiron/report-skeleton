# Лабораторная работа 1

Название: "Разработка драйверов символьных устройств" Вариант 3

Цель работы: Разработать драйвер символьного устройства исходя из требований, предложенных в варианте

## Описание функциональности драйвера

При записи текста в файл символьного устройства осуществляется расчет суммы всех встречающихся в тексте чисел. При многократной записи история сумм сохраняется. Далее при чтении устройства выводится данная история сумм.

## Инструкция по сборке

```bash
make
sudo insmdod ch_drv.ko
sudo chmod 777 /dev/var3
```

## Инструкция пользователя

- Записать последовательность символов в /dev/var3, используя ```echo ... >/dev/var3```
- Вывести сумму чисел, встречающихся в этой последовательности с помощью ```cat /dev/var3```

## Примеры использования
```bash
echo qsdf213a323f >/dev/var3 
cat /dev/var3  # 535,
echo 8a3 >/dev/var3
cat /dev/var3  # 535, 11,
```
